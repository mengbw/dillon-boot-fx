# dillon-boot-fx

javafx权限管理系统UI

## 简介

本项目是用javafx实现的一套权限管理系统UI，毫无保留给个人及企业免费使用。

* 界面参考若依前端(基于 [RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue))
* 后端采用[若依/RuoYi-Cloud](https://gitee.com/y_project/RuoYi-Cloud)
* 前端技术栈:
    * 应用程序结构 [mvvmFX](https://github.com/sialcasa/mvvmFX) ([文档](https://github.com/sialcasa/mvvmFX/wiki))
    * 主题 [atlantafx](https://github.com/mkpaz/atlantafx) ([文档](https://mkpaz.github.io/atlantafx/))
    * 组件库 [MaterialFX](https://github.com/palexdev/MaterialFX)（组件都是用原生的，暂时不用MaterialFX，等待作者重构完成！）
    * 图标库 [ikonli](https://github.com/kordamp/ikonli) ([文档](https://kordamp.org/ikonli/))
    * 动画库 [AnimateFX](https://github.com/Typhon0/AnimateFX) ([文档](https://github.com/Typhon0/AnimateFX/wiki))
    * http库 [OpenFeign](https://github.com/OpenFeign/feign)

## 启动说明

```agsl
    1 在idea右侧栏找到Maven，展开并点击Plugins->sass-cli:watch，会编译出index.css
    2 运行主类org.dillon.fx.App即可
```


# 界面：

### 登录

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/login.jpg)

### 主页

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/home-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/home-light.jpg)


### 用户管理

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/user-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/user-light.jpg)

### 角色管理
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/role-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/role-light.jpg)

### 菜单管理
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/menu-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/menu-light.jpg)

### 部门管理
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/dept-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/dept-light.jpg)

### 岗位管理
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/post-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/post-light.jpg)

### 字典类型
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/dict-type-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/dict-type-light.jpg)

### 字典数据
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/dict-data-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/dict-data-light.jpg)

### 参数管理
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/config-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/config-light.jpg)

### 通知公告
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/notice-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/notice-light.jpg)

### 操作日志
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/operlog-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/operlog-light.jpg)

### 登录日志
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/logininfo-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/logininfo-light.jpg)

### 服务监控
![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/moint-dark.jpg)

![Image text](https://gitee.com/lwdillon/dillon-boot-fx/raw/main/readme/moint-light.jpg)


## 交流群
## QQ群：114697782 QQ群：518914410
